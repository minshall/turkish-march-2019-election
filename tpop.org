* okay, correlate turkish population with winners in last Sunday's election

there's a [[https://www.reddit.com/r/Turkey/comments/b7tw6k/turkish_local_election_results_2019_and/][reddit]] page with the graph i'm wanting to explore.
apparently comes from [[https://i.redd.it/1pki6qkvyosy.jpg][another]] reddit page.  (here is [[https://twitter.com/orhanaydin6/status/1114195865781055488][the tweet]] i saw.)

population source [[https://www.citypopulation.de/Turkey-C20]]
: wget https://www.citypopulation.de/Turkey-C20

per capita income source
https://commons.wikimedia.org/wiki/File:Turkey_per_capita_income_by_province_2011.svg
: wget 'https://commons.wikimedia.org/wiki/File:Turkey_per_capita_income_by_province_2011.svg'
: mv File\:Turkey_per_capita_income_by_province_2011.{svg,html}
(with an extension of .svg, emacs insists on thinking it is, in fact,
a .svg file)

election results source
https://www.dailysabah.com/elections/march-31-2019-turkish-local-elections-results/all-cities/local-election-results
: wget 'https://www.dailysabah.com/elections/march-31-2019-turkish-local-elections-results/all-cities/local-election-results'

hmm, rvest has some problems with dailysabah.  there's a wikipedia
page https://en.wikipedia.org/wiki/2019_Turkish_local_elections
(from outside Turkey)
: wget 'https://en.wikipedia.org/wiki/2019_Turkish_local_elections'

#+BEGIN_SRC R :session R :colnames yes
  library(rvest)                          # Hadley Wickham miracle package

  ## first, get the table of populations
  x <- read_html("Turkey-C20")            # read the file
  y <- html_nodes(x, xpath="//table")     # find all tables in the file
  z <- html_table(y[1])[[1]] # just happen to know this is the right table

  ## get rid of those commas, make integers integers
  for (cname in c("AreaA-L (km²)", "PopulationCensus (C)1990-10-21",
                  "PopulationCensus (C)2000-10-22",
                  "PopulationCensus (C)2011-11-02",
                  "PopulationEstimate (E)2018-12-31")) {
      z[,cname] <- gsub(",", "", z[,cname])
      z[,cname] <- as.integer(z[,cname])
  }
  ## get rid of the Turkey (country) row
  z <- z[-1,]
  ## get rid of odd columns
  z <- z[,-c(1,11,12)]

  ## okay, normalize names of several provinces
  subs <- cbind(pattern=c("Afyonkarahisar (Afyon)", "Elâzığ", "Hakkâri", "Mersin (İçel)"),
                replacement=c("Afyonkarahisar", "Elazığ", "Hakkari", "Mersin"))
  for (i in subs) {
      ignore <- apply(subs, 1, function(s)
          ## \b: because otherwise Afyonhisar keeps getting hisar'd
          z$Name <<- gsub(sprintf("\\b%s\\b", s["pattern"]), s["replacement"], z$Name))
  }

  ## now, table of per-capita incomes (proxy for GDP/province)
  xx <- read_html("File:Turkey_per_capita_income_by_province_2011.html")
  yy <- html_nodes(xx, xpath="//table")
  zz <- html_table(yy[3])[[1]]

  ## now, table of election results
  xxx <- read_html("2019_Turkish_local_elections")
  yyy <- html_nodes(xxx, xpath="//table")
  zzz <- data.frame()
  for (i in 32:36) {
      zzz <- rbind(zzz, html_table(yyy[i])[[1]])
  }

  subs <- cbind(pattern=c("Afyon", "Hakkâri", "Istanbul", "K. Maraş"),
                replacement=c("Afyonkarahisar", "Hakkari", "İstanbul", "Kahramanmaraş"))
  for (i in subs) {
      ignore <- apply(subs, 1, function(s)
          ## \b: because otherwise Afyonhisar keeps getting hisar'd
          zzz$Province <<- gsub(sprintf("\\b%s\\b", s["pattern"]), s["replacement"], zzz$Province))
  }

  ## produce a merged table
  q <- merge(zz, z, by.x="Province", by.y="Name")
  q <- merge(zzz, q, by.x="Province", by.y="Province")

  ## which columns we use (short hand)
  POP <- "PopulationEstimate (E)2018-12-31"
  PERCAPITA <- "Per capita income (TRY)"
  AREA <- "AreaA-L (km²)"

  q[,POP] <- as.numeric(q[,POP])
  q[,PERCAPITA] <- as.numeric(q[,PERCAPITA])
  ## produce a new column of gdp/province
  q$Gdp <- q[,POP]*q[,PERCAPITA]
  ## get total population, area, gdp for the entire country
  tpop <- sum(q[,POP])
  tarea <- sum(q[,AREA])
  tgdp <- sum(q[,"Gdp"])

  ## now, column of coalition identification
  q <- cbind(q, Coalition=c("independent"), stringsAsFactors=FALSE)
  q[q$Elected %in% c("AKP", "MHP"),"Coalition"] <- "People's"
  q[q$Elected %in% c("CHP", "İYİ"),"Coalition"] <- "Nation"

  agg <- aggregate(x=list(Gdp=q$Gdp, Population=q[,POP], Area=q[,AREA]),
                   by=list(Coalition=q$Coalition), FUN=sum)
  agg[,"Pop%"] <- round((agg$Population/tpop)*100, digits=0)
  agg[,"GDP%"] <- round((agg$Gdp/tgdp)*100, digits=0)
  agg[,"Area%"] <- round((agg$Area/tarea)*100, digits=0)
  agg <- agg[,-which(colnames(agg) %in% c("Gdp", "Population", "Area"))]
  agg <- agg[order(agg[,"GDP%"], decreasing=TRUE),]
#+END_SRC

#+RESULTS:






| Coalition   | Pop% | GDP% | Area% |
|-------------+------+------+-------|
| Nation      |   48 |   52 |    25 |
| People's    |   45 |   43 |    63 |
| independent |    7 |    5 |    12 |
